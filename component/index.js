import React, { useEffect, useState, useMemo } from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createDrawerNavigator } from "@react-navigation/drawer";

import { SignIn } from './screen/signInScreen';
import { SignUp } from './screen/signUpScreen';
import { Splash } from './screen/splashScreen';
import { Home } from './screen/homeScreen';


const AuthStack = createStackNavigator();
const SignInStack = createStackNavigator();
const HomeStack = createStackNavigator();

const HomeStackScreen = () =>{
  return(
  <HomeStack.Navigator headerMode="none">
    <HomeStack.Screen name="Home" component={Home}/>
  </HomeStack.Navigator>
  )
}


const SignInScreen = () =>{
  return(
    <SignInStack.Navigator  headerMode="none">
    <SignInStack.Screen name="SignIn" component={SignIn} 
      options= {{
        title: 'Masuk',
        // headerStyle: {
        //   backgroundColor: 'white',
        //   borderBottomColor: 'white'
        // }, 
      }} 
      />
    <SignInStack.Screen name="Home" component={HomeStackScreen}/>
    </SignInStack.Navigator>
  )
}


const App = () =>{
  const [isLoading, setIsLoading] = useState(true);
  
  //mengatur waktu splashscreen selama 1 detik
  useEffect(() => {
    setTimeout(() => {
      setIsLoading(false)
    }, 1000)
  }, []);

  //jika loading true maka mengeksekusi fungi Splash
  if (isLoading) {
    return <Splash />
  }

  return(
    <NavigationContainer>
      <AuthStack.Navigator headerMode="none">
        <AuthStack.Screen name="SignUp" component={SignUp} 
        options= {{
          title: ' ',
        }} />
        <AuthStack.Screen name="SignIn" component={SignInScreen}  />
      </AuthStack.Navigator>
    </NavigationContainer>

  ) 
}

export default App;

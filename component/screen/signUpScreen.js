import React, { Component, useState } from 'react';
import { 
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Button,
  Image,
  Dimensions,
 } from 'react-native';
 import AntDesign from 'react-native-vector-icons/AntDesign';
 import Icon from 'react-native-vector-icons/MaterialIcons';
 import { AppLoading } from 'expo';
 import { useFonts } from 'expo-font'

export const SignUp = ({ navigation }) => {
  // let [fontsLoaded] = useFonts({
  //   "Montserrat" : require('../../assets/font/Montserrat-Regular.ttf')
  // });
  
  // if (!fontsLoaded) {
  //   return <AppLoading />;
  // }
   // Mengatur Visibility Password
	const [ hideShow, setHideShow ] = useState({
		icEye: 'visibility-off',
		isPassword: true
  });
  
  	// Merubah password menjadi text
	const changePwdType = () => {
		setHideShow({
			icEye: hideShow.isPassword ? 'visibility' : 'visibility-off',
			isPassword: !hideShow.isPassword
		});
	};


  const [ username, onChangeUsername ] = useState('');
  const [ email, onChangeEmail ] = useState('');
  const [ phoneNumber, onChangePhoneNumber ] = useState('');
  const [ password, onChangePassword ] = useState('');


  return(
  <View style={styles.container}>
    <View style={styles.body}>
      <View>
        <Text style={{fontSize: 14 }}>Selamat Datang</Text>
        <Text style={styles.titleBody}>Rawat Lingkunganmu
          Dan Daftar Sekarang di 
          Sampahku !
        </Text>
      </View>
      <View>
        <View style={{marginTop: 30}}>
          <Text style={styles.formName}>Nama</Text>
          <View style={styles.formInput}>
              <TextInput
                style={styles.textInput}
                onChangeText={text => onChangeUsername(text)}
                value={username}
              />
          </View>
        </View>
        <View style={{marginTop: 10}}>
          <Text style={styles.formName}>Email</Text>
          <View style={styles.formInput}>
              <TextInput
                style={styles.textInput}
                onChangeText={text => onChangeEmail(text)}
                value={email}
              />
          </View>
        </View>
        <View style={{marginTop: 10}}>
          <Text style={styles.formName}>Nomor Handphone</Text>
          <View style={styles.formInput}>
              <TextInput
                style={styles.textInput}
                onChangeText={text => onChangePhoneNumber(text)}
                value={phoneNumber}
                keyboardType={'numeric'}
              />
          </View>
        </View>
        <View style={{marginTop: 10}}>
          <Text style={styles.formName}>Kata Sandi</Text>
          <View style={styles.formInput}>
              <TextInput
                style={styles.textInput}
                secureTextEntry={hideShow.isPassword}
                onChangeText={text => onChangePassword(text)}
                value={password}
              />
              <TouchableOpacity onPress={changePwdType} style={styles.IconEye}>
                <Icon name={hideShow.icEye} size={20}/>
              </TouchableOpacity>
          </View>
        </View>
        <TouchableOpacity 
          onPress={() => navigation.push("SignIn")}
          style={styles.button}>
           <Text style={{color: 'white', fontSize: 14}}>Daftar</Text>
        </TouchableOpacity>
        <View style={{flexDirection: 'row', justifyContent: 'center', marginTop: 10}}>
          <Text>Sudah memiliki akun? </Text>
          <TouchableOpacity 
            onPress={() => navigation.push("SignIn")}
          >
            <Text style={{ color:"#14AB4E", fontWeight: 'bold'}}>Masuk</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  </View>
  )
}

const widthScreen = Dimensions.get('window').width;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:'white',
    // fontFamily: 'Montserrat',
  },
  body: {
    flex: 1,
    paddingHorizontal: 24,
    marginTop: 30
  },
  button:{
    marginTop: 20,
    height:35, 
    backgroundColor: '#14AB4E', 
    justifyContent: 'center', 
    flexDirection:'row', 
    alignItems: 'center',
    borderRadius: 6

  },
  titleBody: {
    fontSize: 24,
    fontWeight: 'bold'
  },
  fogotPassword: {
    flexDirection:'row',
    marginTop: 10,
    justifyContent: 'flex-end'
  },
  formName:{
    fontSize: 12
  },
  formInput: {
    fontSize: 15,
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomColor: 'gray', 
  },
  textInput: { 
    height: 40,  
    width: widthScreen, 
    fontSize: 16,
    borderBottomColor: '#000000',
    borderBottomWidth: 1,
  },
  otherAccount:{
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems:'center',
    // marginLeft: 15
  },
  IconEye:{
    position: 'absolute',
    bottom: 0, 
    top: 5, 
    left: widthScreen-80, 
    right: 0
  }
  
});
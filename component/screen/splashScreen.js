import React, { Component, useState } from 'react';
import { LinearGradient } from 'expo-linear-gradient';

import { 
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions
 } from 'react-native';
 import { Ionicons } from '@expo/vector-icons';
 import Icon from 'react-native-vector-icons/MaterialIcons';

 const windowWidth = Dimensions.get('window').width;
 const windowHeight = Dimensions.get('window').height;

 export const Splash = () =>{
   return(
     <View style={styles.container}>
         <LinearGradient
          // Background Linear Gradient
          colors={['#F8FEFC', '#CDF9EC']}
          style={{
            position: 'absolute',
            left: 0,
            right: 0,
            top: 0,
            height: windowHeight,
          }}
        />
       <View style={styles.body}>
        <Image source={require('../images/logo.png')} style={styles.logo}/>
      </View>
     </View>
   )
 }

 const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  body:{
    alignItems: 'center',
    justifyContent: 'center',

  },
  logo: {
    width: 200, 
    height: 140,
  }
 })
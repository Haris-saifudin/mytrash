import React, { Component, useState } from 'react';
import { 
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Button,
  Image,
  Dimensions,
 } from 'react-native';
 import AntDesign from 'react-native-vector-icons/AntDesign';
 import Icon from 'react-native-vector-icons/MaterialIcons';
 
export const SignIn = ({ navigation }) => {
  // Mengatur Visibility Password
	const [ hideShow, setHideShow ] = useState({
		icEye: 'visibility-off',
		isPassword: true
  });
  
  	// Merubah password menjadi text
	const changePwdType = () => {
		setHideShow({
			icEye: hideShow.isPassword ? 'visibility' : 'visibility-off',
			isPassword: !hideShow.isPassword
		});
	};


  const [ email, onChangeEmail ] = useState('');
  const [ password, onChangePassword ] = useState('');


  return(
  <View style={styles.container}>
    <View style={styles.body}>
      {/* <View>
        <Text style={styles.titleBody}>Welcome Back</Text>
        <Text style={{fontSize: 12 }}>Sign in to continue</Text>
      </View> */}

      <View style={{flexDirection: 'row', alignItems: 'center', justifyContent:'center'}}>
        <Image source={require('../images/bebas sampah.png')} style={{width: 300, height: 73, alignItems: "center", justifyContent: "center", flexDirection: 'row'}} />
      </View>
      <View style={styles.form}>
        <View style={{marginTop: 10}}>
          <Text style={styles.formName}>Email</Text>
          <View style={styles.formInput}>
              <TextInput
                style={styles.textInput}
                onChangeText={text => onChangeEmail(text)}
                value={email}
              />
          </View>
        </View>
        <View style={{marginTop: 10}}>
          <Text style={styles.formName}>Kata Sandi</Text>
          <View style={styles.formInput}>
              <TextInput
                style={styles.textInput}
                secureTextEntry={hideShow.isPassword}
                onChangeText={text => onChangePassword(text)}
                value={password}
              />
              <TouchableOpacity onPress={changePwdType} style={styles.IconEye}>
                <Icon name={hideShow.icEye} size={20}/>
              </TouchableOpacity>
          </View>
        </View>
        <TouchableOpacity style={styles.fogotPassword}>
          <Text style={{fontSize : 12}}>lupa kata sandi</Text>
        </TouchableOpacity>
        <TouchableOpacity 
          style={styles.button} 
          onPress={() => navigation.push("Home")} 
        >
           <Text style={{color: 'white', fontSize: 14}}>Masuk</Text>
        </TouchableOpacity>
        <View style={styles.separator}>
          <Text style={{fontSize:12}}>- atau masuk dengan -</Text>
        </View>
        <View style={styles.account}>
            <TouchableOpacity style={styles.otherAccount}>
              <AntDesign name="facebook-square" size={20} style={{color: '#0F90F0'}} />
              <Text style={styles.nameOtherAccount}>Facebook</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.otherAccount}>
              <Image source={{uri: 'https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Google_%22G%22_Logo.svg/768px-Google_%22G%22_Logo.svg.png'}}
                style={{width: 18, height: 18}}
              />
              <Text style={styles.nameOtherAccount}>Google</Text>
            </TouchableOpacity>
          </View>
      </View>
    </View>
  </View>
  )
}
 
const widthScreen = Dimensions.get('window').width;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:'white',
    // fontFamily: 'Montserrat',
  },
  body: {
    flex: 1,
    paddingHorizontal: 24,
    marginTop: 30
  },
  button:{
    marginTop: 20,
    height:35, 
    backgroundColor: '#14AB4E', 
    justifyContent: 'center', 
    flexDirection:'row', 
    alignItems: 'center',
    borderRadius: 6
  },
  titleBody: {
    fontSize: 30,
    fontWeight: 'bold'
  },
  form:{
    marginTop: 20,
    // borderRadius: 10,
    // shadowColor: "#000",
    // shadowOffset: {
    //   width: 0,
    //   height: 2,
    // },
    // shadowOpacity: 0.25,
    // shadowRadius: 3.84,
  },  
  fogotPassword: {
    flexDirection:'row',
    marginTop: 10,
    justifyContent: 'flex-end',
    fontSize: 12
  },
  formName:{
    fontSize: 12
  },
  formInput: {
    fontSize: 15,
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomColor: 'gray', 
  },
  textInput: { 
    height: 40,  
    width: widthScreen, 
    fontSize: 16,
    borderBottomColor: '#000000',
    borderBottomWidth: 1,
  },
  otherAccount:{
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems:'center',
  },
  nameOtherAccount :{ 
    fontSize:14, 
    marginLeft: 10
  },
  account:{
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: 12,
  },
  separator:{
    flexDirection: 'column',
    justifyContent: 'center', 
    alignItems:'center', 
    marginTop: 12,
    fontSize: 12
  },
  IconEye:{
    position: 'absolute',
    bottom: 0, 
    top: 5, 
    left: widthScreen-80,
    right: 0
  }
  
});